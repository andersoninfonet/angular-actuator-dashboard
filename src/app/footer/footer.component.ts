import { Component, OnInit } from '@angular/core';
import { faChartLine, IconDefinition } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  faChartLine: IconDefinition = faChartLine;
  actualYear: number = new Date().getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

}
