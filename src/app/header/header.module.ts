import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MenubarModule } from 'primeng/menubar';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    MenubarModule,
    TabViewModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
