import { Component, OnInit } from '@angular/core';
import { faChartLine, IconDefinition } from "@fortawesome/free-solid-svg-icons";
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faChartLine: IconDefinition = faChartLine;
  items: MenuItem[] = [];


  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        label: "Home",
        icon: "pi pi-home"
      }
    ]
  }

}
